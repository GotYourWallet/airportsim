#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <time.h>
#include <limits.h>

#define MAX 7
#define ARRIVE 0
#define DEPART 1

struct plane {
	int id;
	int tm;
};

/*
 * QUEUE
 */

struct queue {
	
	int count;
	int front;
	int rear;
	struct plane p[MAX];
};

void initqueue(struct queue *);
void addqueue(struct queue *, struct plane);
struct plane delqueue(struct queue *);
int size(struct queue);
int empty(struct queue);
int full(struct queue);

void initqueue(struct queue *planequeue) {
	planequeue->count = 0;
	planequeue->front = 0;
	planequeue->rear = -1;
}

void addqueue(struct queue *planequeue, struct plane item) {
	if (planequeue->count >= MAX) {
		printf("\nThe plane queue is full!\n");
		return;
	}
	
	//calculate the sport for the rearmost plane in the queue then set that spot to that plane in the queue 
	planequeue->rear = (planequeue->rear + 1) % MAX;
	planequeue->p[planequeue->rear] = item;
	
	(planequeue->count)++;
}

struct plane delqueue(struct queue *planequeue) {
	struct plane p1;
	
	if (planequeue->count <= 0) {
		printf("\nThe plane queue is empty!\n");
		p1.id = 0;
		p1.tm = 0;
	}
	else {
		p1 = planequeue->p[planequeue->front];
		planequeue->front = (planequeue->front+1) % MAX;
		
		(planequeue->count)--;
	}
	return p1;
}

int size(struct queue q) {
	return q.count;
}

int empty(struct queue q) {
	//if the q.count <= 0, return 1
	//if the q.count > 0, return 0
	return (q.count <= 0);
}

int full (struct queue q) {
	return (q.count >= MAX);
}

/*
 * AIRPORT
 */

struct airport
{
	struct queue landing;
    struct queue takeoff;
    struct queue *pl;
    struct queue *pt;
    int idletime;
    int landwait;
	int takeoffwait;
    int nland;
	int nplanes;
	int nrefuse;
	int ntakeoff;
    struct plane pln;
};

void initairport(struct airport *);
void start(int *, double *, double *);
void newplane(struct airport *, int, int);
void refuse(struct airport *, int);
void land (struct airport *, struct plane, int);
void fly (struct airport *, struct plane, int);
void idle (struct airport *, int);
void conclude(struct airport *, int);
int randomnumber(double);
void apaddqueue(struct airport *, char);
struct plane apdelqueue(struct airport *, char);
int apsize(struct airport *, char);
int apfull(struct airport *, char);
int apempty(struct airport *, char);
void myrandomize();

void initairport(struct airport *ap) {
	initqueue(&(ap->landing));
	initqueue(&(ap->takeoff));
		
	//saving the adresses in queue* because lazy?
	ap->pl = &(ap->landing);
	ap->pt = &(ap->takeoff);
	
	ap->nplanes =  0;
	ap->nland = 0;
	ap->ntakeoff = 0;
	ap->nrefuse = 0;
	
	ap->landwait= 0;
	ap->takeoffwait = 0;
	ap->idletime = 0;	
}

void start (int *endtime, double *expectarrive, double *expectdepart ) {
	int flag = 0;
	char wish = 0;
	
	printf ("\nProgram that simulates an airport with only one runway.");
    printf ("\nIn each unit of time one plane can either land or depart.");
    printf ("\nUp to %d planes can be waiting to land or take off at any time.", MAX);
	
	do {
		printf ("\nHow many units of time will the simulation run? ");
		scanf ("%d", endtime); //scaf needs a memory adress in the argument

		if (endtime <= 0) {
			printf ("\nThe number of time units must be bigger than zero.");
			flag = 0;
		} else {
			flag = 1;
		}
		
	}while (flag == 0);
	
	
	
	myrandomize();
	
	do {
		printf ("\nExpected number of arrivals per unit time? ");
        scanf ("%lf", expectarrive);
        printf ("\nExpected number of departures per unit time? ");
        scanf ("%lf", expectdepart);
		
		if (*expectarrive < 0.0 || *expectdepart < 0.0) {
			printf ("These numbers must be nonnegativ.\n");
			flag = 0;
		} else {
			if (*expectarrive - *expectdepart > 1.0) {
				printf ("The airport will become saturated . Read new numbers? ");
				while (getchar () != '\n'); //flush input stream until carriage return
				scanf ("%c", &wish);
				if (tolower (wish) == 'y') {
					flag = 0;
				} else {
					flag = 1;
				}
			} else {
				flag = 1;
			}
		} 
	} while (flag == 0);
}

void newplane (struct airport *ap, int curtime, int action) {
	(ap->nplanes)++;
	ap->pln.id = ap->nplanes;
	ap->pln.tm = curtime;
	
	switch (action) {
		case ARRIVE:
			printf("\nPlane %d ready to land.", ap->pln.id);
			break;
		case DEPART:
			printf("\nPlane %d ready for takeoff.", ap->pln.id);
			break;
		default:
			;
	}
}

void refuse(struct airport *ap, int action) {
	switch (action) {
			case ARRIVE:
			printf("\n\tPlane %d directed to another airport!\n", ap->pln.id);
			break;
		case DEPART:
			printf("\n\tPlane %d told to try again later", ap->pln.id);
			break;
		default:
			;
	}
	(ap->nrefuse)++;	
}

void land(struct airport *ap, struct plane pln, int curtime) {
	int wait = curtime - pln.tm;
	
	printf ("\n%d: Plane %d landed ", curtime, pln.id);
	printf("and was in queue for %d units.", wait);
	(ap->nland)++;
	(ap->landwait) += wait;
}

void fly(struct airport *ap, struct plane pln, int curtime) {
	int wait = curtime - pln.tm;
	
	printf ("\n%d: Plane %d took of ", curtime, pln.id);
	printf("and was in queue for %d units.", wait);
	(ap->ntakeoff)++;
	(ap->takeoffwait) += wait;
}

void idle(struct airport *ap, int curtime) {
	printf("\n%d: Runway is idle.", curtime);
	(ap->idletime)++;
}

void conclude(struct airport *ap, int endtime) {
	printf ("\n\n\tThe simulation has concluded after %d units.", endtime);
    printf ("\n\tTotal number of planes processed: %d", ap->nplanes);
    printf ("\n\tNumber of planes landed: %d", ap->nland);
    printf ("\n\tNumber of planes taken off: %d", ap->ntakeoff);
    printf ("\n\tNumber of planes refused use: %d", ap->nrefuse);
    printf ("\n\tNumber left ready to land: %d", apsize(ap, 'l'));
    printf ("\n\tNumber left ready to take off: %d", apsize(ap, 't'));

	if (endtime > 0) {
		printf("\n\tPercentage of time runway idle: %lf", ((double) ap->idletime / endtime) * 100.0);
	}
	if (ap->nland > 0) {
		printf("\n\tAverage wait time to land: %lf", ((double) ap->landwait / ap->nland ));
	}
	if (ap->ntakeoff > 0)
        printf ("\n\tAverage wait time to take off: %lf", ((double) ap->takeoffwait / ap->ntakeoff));
}

int randomnumber(double expectedvalue) {
	int n = 0;
	double em = exp (-expectedvalue);
	double x = rand();
	
	x = x / (double) INT_MAX;
	
	while (x > em) {
	 n++;
	 x *= rand() / (double) INT_MAX;
	}
	
	return n;
}

void apaddqueue(struct airport *ap, char type) {
	switch (tolower (type)) {
		case 'l':
			addqueue(ap->pl, ap->pln);
			break;
		case 't':
			addqueue(ap->pt, ap->pln);
			break;
		default:
			;
	}
}

struct plane apdelqueue(struct airport *ap, char type) {
	struct plane p1;
	
	switch (tolower (type)) {
		case 'l':
			p1 = delqueue(ap->pl);
			break;
		case 't':
			p1 = delqueue(ap->pt);
			break;
		default:
			;
	}
	return p1;
}

int apsize(struct airport *ap, char type) {
    int n;
	
	switch (tolower (type)) {
        case'l':
            n = size(*(ap->pl));
			break;
		case't':
            n = size(*(ap->pt));
			break;
		default:
			;
			n = 0;
    }
	
	return n ;
}

int apfull ( struct airport *ap, char type ) {
     int n;
	
	switch (tolower (type)) {
        case'l':
            n = full(*(ap->pl));
			break;
		case't':
            n = full(*(ap->pt));
			break;
		default:
			;
			n = 0;
    }
	
	return n ;
}

int apempty ( struct airport *ap, char type ) {
     int n;
	
	switch (tolower (type)) {
        case'l':
            n = empty(*(ap->pl));
			break;
		case't':
            n = empty(*(ap->pt));
			break;
		default:
			;
			n = 0;
    }
	
	return n ;
}

void myrandomize() {
	srand ((unsigned int) (time (NULL) % 10000));
}

/*
 *MAIN
 */

 void main() {
	struct airport ap = {0};
	int i = 0;
	int pri = 0;
	int curtime = 0;
	int endtime = 0;
	double expectarrive = 0;
	double expectdepart = 0;
	struct plane tempPlane = {0};
	
	printf("\n\n\n");
	
	initairport (&ap);
	
	//Title screen
	start (&endtime, &expectarrive, &expectdepart);
	
	//Simulation starts
	for (curtime = 1; curtime <= endtime; curtime++) {
		// Generate a random number 
		pri = randomnumber(expectarrive);
		// Generate that number of landing and flying planes
		for (i = 1; i <= pri; i++) {
			newplane(&ap, curtime, ARRIVE);
			if (apfull(&ap, 'l')) {
				refuse(&ap, ARRIVE);
			} else {
				apaddqueue(&ap,'l');
			}
		}
		pri = randomnumber(expectdepart);
		for (i = 1; i <= pri; i++) {
			newplane(&ap, curtime, DEPART);
			if (apfull(&ap, 't')) {
				refuse(&ap, DEPART);
			} else {
				apaddqueue(&ap,'t');
			}
		}
		
		/* if landing queue not empty land a plane 
		 * else if takeoff queue not empty fly a plane
		 * else idle 
		 */
		if (!(apempty(&ap,'l'))) {
			tempPlane = apdelqueue(&ap,'l');
			land(&ap,tempPlane,curtime);
		} else if (!(apempty(&ap,'t'))) {
			tempPlane = apdelqueue(&ap,'t');
			fly(&ap,tempPlane,curtime);
		} else {
			idle (&ap, curtime);
		}
	
		while (getchar () != '\n');
		getchar ();		
	}
	
	//Ending Screen
	conclude(&ap, endtime);

	while (getchar () != '\n');
	getchar ();
	

}










